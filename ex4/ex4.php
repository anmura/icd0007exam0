<?php

require_once 'functions.php';

$conn = getConnectionWithData('data.sql');

$stmt = $conn->prepare('select student.name, grade.grade from student, grade where student.id = grade.id');

$stmt->execute();

$result = [];

foreach ($stmt as $smth){
    if (!isset($result[$smth['name']])){
        $result[$smth['name']] = [];}
    array_push($result[$smth['name']], $smth['grade'] );

}

foreach ($result as $name => $grades) {
    $std = standardDeviation($grades);
    print "$name: $std\n";
}
